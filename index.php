<?php 
namespace App;

require_once 'vendor/autoload.php';
use Omnipay\Omnipay as Omnipay;

$gateway = Omnipay::create('SipsPayPage');


/*
define('SPP_MERCHANTID', '002001000000001');
define('SPP_SECRETKEY', '002001000000001_KEY1');
define('SPP_URL', 'https://payment-webinit.simu.sips-atos.com');

$gateway->setMerchantId(SPP_MERCHANTID);
$gateway->setSecretKey(SPP_SECRETKEY);
$gateway->setUrl(SPP_URL);

*/

if(isset($_GET["return"])){
    // Send completePurchase request 
    $request = $gateway->completePurchase();
    $response = $request->send();

    if ($response->isSuccessful()) {
        // DO your store logic.
        
        $bankTransactionRef = $response->getTransactionReference();
        $websiteOrderId = $response->getTransactionId();
    } elseif ($response->isPending()) {
        // Do temporary things until we get a success/failed tranaction response.
    } else {
        echo $response->getMessage();
    }
    var_dump($response);
}else{


    $card = new \Omnipay\SipsPayPage\OffsiteCreditCard();
    $card->setEmail('test@test.com');

    // Send purchase request
    $request = $gateway->purchase(
        [
            'clientIp' => '213.32.73.200',
            'amount' => '10.00',
            'currency' => 'EUR',
            'description' => 'Description field',
            'language' => 'fr',
            'returnUrl' => 'http://localhost/test/index.php?return=1',
            'notifyUrl' => 'http://localhost/test/return.php',
            'cancelUrl' => 'http://localhost/test/return.php',
            'card'      => $card,
            'transactionReference' => 'gin' . rand(6,1000) ,
            'paymentMeanBrandList'  => 'VISA,MASTERCARD,CB'
        ]
    );

    $response = $request->send();

    if ($response->isRedirect()) {
        $response->redirect(); 

    }

}